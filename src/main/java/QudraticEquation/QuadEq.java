package QudraticEquation;

public class QuadEq {
    private double a,b,c;
    private double EPS = 1e-10;
    public QuadEq(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double[] solve(){
        if(Math.abs(a) < EPS){
            if(Math.abs(b) < EPS){
                throw new ArithmeticException("Equation is incorrect");
            }
            return new double[]{c / b};
        }
        double d = b * b - 4 * a * c;
        if(d < 0){
            throw new ArithmeticException("discriminant is less then 0,no roots over R");
        } else if(Math.abs(d) < EPS){
            return new double[]{-b / (2 * a)};
        }
        return new double[]{(-b + Math.sqrt(d)) / (2 * a), (-b - Math.sqrt(d)) / (2 * a)};
    }
}
