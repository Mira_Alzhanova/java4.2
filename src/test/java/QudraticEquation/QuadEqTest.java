package QudraticEquation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QuadEqTest {

    @Test
    void solve() {
        Assertions.assertThrows(ArithmeticException.class, () -> new QuadEq(1d,2d,3d).solve());
        Assertions.assertThrows(ArithmeticException.class, () -> new QuadEq(0d,0d,1).solve());

        QuadEq qe1 = new QuadEq(1d,0d,-4d);
        Assertions.assertEquals(qe1.solve()[0],2d,1e-10);
        Assertions.assertEquals(qe1.solve()[1],-2d,1e-10);

        QuadEq qe2 = new QuadEq(0d,1d,1d);
        Assertions.assertEquals(qe2.solve()[0],1d,1e-10);

        QuadEq qe3 = new QuadEq(1d,1d,0);
        Assertions.assertEquals(qe3.solve()[0],0f,1e-10);
        Assertions.assertEquals(qe3.solve()[1],-1f,1e-10);
    }
}